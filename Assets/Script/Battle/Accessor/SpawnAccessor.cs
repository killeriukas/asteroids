﻿using UnityEngine;

public class SpawnAccessor : MonoBehaviour {

    public Transform playerSpawnTransform;

    public GameObject smallAsteroidPrefab;
    public GameObject mediumAsteroidPrefab;
    public GameObject bigAsteroidPrefab;

    public AudioSource sfxProjectileExplosion;
}
