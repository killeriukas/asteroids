﻿using TMI.State;
using UnityEngine;

public class AsteroidBehaviour : BaseFlyingObjectBehaviour, IAsteroidBehaviour {

    public enum Size {
        None,
        Small,
        Medium,
        Big
    }

    [SerializeField]
    private Size _splitSize = Size.None;

    [SerializeField]
    private Vector2 screenPivotOffset;

    [SerializeField]
    private int _destructionScore;

    public int destructionScore => _destructionScore;
    public Size splitSize => _splitSize;
    public Vector2 position => transform.position;

    private GameObjectPool<AsteroidBehaviour> asteroidPool;

    protected override IStateMachine CreateStateMachine() {
        return StateMachine.Create(new FlyingObjectFlyingState(initializer, this));
    }

    public override void PlayExplosionVfx() {
    }

    protected override Vector2 mirrorOffset => screenPivotOffset;

    public override void TurnOff() {
        asteroidPool.Release(this);
        base.TurnOff();
    }

    public void Initialize(Vector2 startingPosition, Vector2 randomDirection, GameObjectPool<AsteroidBehaviour> pool) {
        base.Initialize(startingPosition, randomDirection);
        this.asteroidPool = pool;
    }

    public override BaseFlyingObjectState CreateExplodedState() {
        return new AsteroidExplodedState(initializer, this);
    }
}
