﻿using UnityEngine;

public interface IAsteroidBehaviour : IFlyingObjectBehaviour {
    AsteroidBehaviour.Size splitSize { get; }
    Vector2 position { get; }
    int destructionScore { get; }
}