﻿using TMI.Notification;

public class AsteroidExplodedNotification : INotification {

    public readonly int score;

    public AsteroidExplodedNotification(int score) {
        this.score = score;
    }

}