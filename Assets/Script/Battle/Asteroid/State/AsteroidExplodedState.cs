﻿using TMI.Core;

public class AsteroidExplodedState : FlyingObjectExplodedState {

    private SpawnManager spawnManager;

    public AsteroidExplodedState(IInitializer initializer, IAsteroidBehaviour proxy) : base(initializer, proxy) {
        this.spawnManager = initializer.GetManager<SpawnManager>();
    }

    public override void Enter() {
        base.Enter();

        IAsteroidBehaviour asteroidBehaviour = (IAsteroidBehaviour)proxy;
        if(asteroidBehaviour.splitSize != AsteroidBehaviour.Size.None) {
            spawnManager.SpawnSplitAsteroids(asteroidBehaviour.splitSize, asteroidBehaviour.position);
        }

        Trigger(new AsteroidExplodedNotification(asteroidBehaviour.destructionScore));
    }

}
