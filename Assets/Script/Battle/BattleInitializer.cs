﻿using TMI.AssetManagement;
using TMI.Core;
using TMI.UI;
using UnityEngine;

public class BattleInitializer : BaseCacheUIMiniInitializer {

    [SerializeField]
    private SpawnAccessor spawnAccessor;

    [SerializeField]
    private GameObject playerPrefab;

    [SerializeField]
    private Camera orthographicCamera;

    private BattleManager battleManager;
    private SpawnManager spawnManager;

    protected override ISceneGroup CreateUIScenesCache() {
        ISceneGroup sceneGroup = SceneGroup.Create();
        sceneGroup.Add("ui_scene_battle");
        return sceneGroup;
    }

    protected override void OnUIScenesCached() {
        spawnManager.Initialize(spawnAccessor, 5);
        battleManager.Initialize(playerPrefab, spawnAccessor, orthographicCamera);

        BattleUIController battleUIController = uiManager.LoadUI<BattleUIController>();
        battleUIController.Show();

        LoadingScreenUIController loadingScreen = uiManager.LoadUI<LoadingScreenUIController>();
        loadingScreen.Hide();
    }

    protected override void RegisterManagers(IAcquirer acquirer) {
        base.RegisterManagers(acquirer);
        battleManager = acquirer.AcquireManager<BattleManager>();
        spawnManager = acquirer.AcquireManager<SpawnManager>();
    }

    protected override void OnDestroy() {
        uiManager.UnloadUIScene("ui_scene_battle");
        base.OnDestroy();
    }

}