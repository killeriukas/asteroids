﻿using System;
using TMI.Core;
using TMI.Helper;
using TMI.State;
using UnityEngine;

public class BattleManager : BaseNotificationManager {

    private SpacecraftBehaviour spacecraftBehaviour;
    private IStateMachine stateMachine;

    private PlayerProfile _playerProfile;
    public IPlayerProfile playerProfile => _playerProfile;

    public Vector2 halfArenaSize { get; private set; }
    public AudioSource sfxProjectileExplosion { get; private set; }

    protected override void Awake() {
        base.Awake();

        const int startingHealthAmount = 3;
        _playerProfile = new PlayerProfile(startingHealthAmount);
    }

    public override void Setup(IInitializer initializer, bool isNew) {
        base.Setup(initializer, isNew);

        Listen<AsteroidExplodedNotification>(this, OnAsteroidExploded);
        Listen<SpacecraftExplodedNotification>(this, OnSpacecraftExploded);
    }

    private void OnSpacecraftExploded(SpacecraftExplodedNotification notification) {
        _playerProfile.ConsumeHealth();

        if(0 == _playerProfile.healthAmount.value) {
            Trigger(new AllPlayerLivesLostNotification());
        }
    }

    private void OnAsteroidExploded(AsteroidExplodedNotification notification) {
        _playerProfile.AddScore(notification.score);
    }

    public void Initialize(GameObject playerPrefab, SpawnAccessor spawnAccessor, Camera mainCamera) {
        halfArenaSize = new Vector2(mainCamera.orthographicSize * mainCamera.aspect, mainCamera.orthographicSize);
        sfxProjectileExplosion = spawnAccessor.sfxProjectileExplosion;

        spacecraftBehaviour = HierarchyHelper.InstantiateAndSetupBehaviour<SpacecraftBehaviour>(initializer, playerPrefab);
        spacecraftBehaviour.transform.SetParent(spawnAccessor.playerSpawnTransform, false);

        stateMachine = StateMachine.Create(new BattleCountdownState(initializer));

        RegisterUpdate();
    }

    protected override ExecutionManager.Result OnUpdate() {
        stateMachine.Update();
        return ExecutionManager.Result.Continue;
    }

    public bool CalculateMirrorPositionOnEdge(Vector2 currentPosition, Vector2 offset, out Vector2 newPosition) {

        bool hasChanged = false;
        const float yError = 1f;

        newPosition = currentPosition;

        if(newPosition.x > halfArenaSize.x + offset.x) {
            newPosition.x = -halfArenaSize.x - offset.x + yError;
            hasChanged = true;
        }

        if(newPosition.x < -halfArenaSize.x - offset.x) {
            newPosition.x = halfArenaSize.x + offset.x - yError;
            hasChanged = true;
        }

        if(newPosition.y > halfArenaSize.y + offset.y) {
            newPosition.y = -halfArenaSize.y - offset.y + yError;
            hasChanged = true;
        }

        if(newPosition.y < -halfArenaSize.y - offset.y) {
            newPosition.y = halfArenaSize.y + offset.y - yError;
            hasChanged = true;
        }

        return hasChanged;
    }

    protected override void OnDestroy() {
        UnregisterUpdate();
        base.OnDestroy();
    }


}