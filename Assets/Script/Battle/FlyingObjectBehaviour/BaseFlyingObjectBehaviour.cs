﻿using System;
using TMI.Core;
using TMI.Helper;
using TMI.Notification;
using TMI.State;
using UnityEngine;

public abstract class BaseFlyingObjectBehaviour : BaseNotificationBehaviour, IFlyingObjectBehaviour, IUpdatable {

    [SerializeField]
    private float speed;

    [SerializeField]
    private Collider2D _collider;

    private Vector2 forward;
    private bool isFlyingObjectActive = false;

    private IStateMachine stateMachine;
    private IExecutionManager executionManager;
    protected BattleManager battleManager { get; private set; }

    public event Action<Collider2D> onTriggerEnter;
    public Collider2D mainCollider => _collider;

    public override void Setup(IInitializer initializer) {
        base.Setup(initializer);
        executionManager = initializer.GetManager<ExecutionManager, IExecutionManager>();
        battleManager = initializer.GetManager<BattleManager>();
    }

    private ExecutionManager.Result OnUpdate() {
        stateMachine.Update();
        return ExecutionManager.Result.Continue;
    }

    public void Initialize(Vector2 startingPosition, Vector2 forward) {
        this.transform.position = startingPosition;
        this.forward = forward;

        stateMachine = CreateStateMachine();

        executionManager.Register(this, OnUpdate);
    }

    public abstract void PlayExplosionVfx();
    protected abstract IStateMachine CreateStateMachine();
    public abstract BaseFlyingObjectState CreateExplodedState();
    protected virtual Vector2 mirrorOffset => Vector2.zero;

    private void FixedUpdate() {
        if(isFlyingObjectActive) {
            transform.Translate(forward * speed * Time.deltaTime);

            //if position is outside of screen, mirror it to the other side
            Vector2 newPosition;
            bool isMirrorRequired = battleManager.CalculateMirrorPositionOnEdge(transform.position, mirrorOffset, out newPosition);
            if(isMirrorRequired) {
                transform.position = newPosition;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        onTriggerEnter?.Invoke(collision);
    }

    public virtual void TurnOn() {
        isFlyingObjectActive = true;
        _collider.enabled = true;
    }

    public virtual void TurnOff() {
        _collider.enabled = false;
        executionManager.Unregister(this);
        isFlyingObjectActive = false;
        GeneralHelper.DisposeAndMakeDefault(ref stateMachine);
    }

    protected override void OnDestroy() {
        if(isFlyingObjectActive) {
            executionManager.Unregister(this);
        }
        base.OnDestroy();
    }

}