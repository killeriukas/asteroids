﻿using System;
using TMI.Pattern;
using UnityEngine;

public interface IFlyingObjectBehaviour : IProxy {
    void TurnOn();
    void TurnOff();
    event Action<Collider2D> onTriggerEnter;

    void PlayExplosionVfx();
    BaseFlyingObjectState CreateExplodedState();
    Collider2D mainCollider { get; }
}