﻿using TMI.Core;
using TMI.State;

public abstract class BaseFlyingObjectState : BaseStateWithProxy<IFlyingObjectBehaviour> {

    protected BaseFlyingObjectState(IInitializer initializer, IFlyingObjectBehaviour proxy) : base(initializer, proxy) {
    }

}
