﻿using TMI.Core;

public class FlyingObjectExpiredState : BaseFlyingObjectState {

    public FlyingObjectExpiredState(IInitializer initializer, IFlyingObjectBehaviour proxy) : base(initializer, proxy) {
    }

    public override void Enter() {
        base.Enter();
        proxy.TurnOff();
    }

}
