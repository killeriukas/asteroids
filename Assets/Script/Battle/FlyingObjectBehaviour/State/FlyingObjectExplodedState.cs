﻿using TMI.Core;

public class FlyingObjectExplodedState : BaseFlyingObjectState {

    public FlyingObjectExplodedState(IInitializer initializer, IFlyingObjectBehaviour proxy) : base(initializer, proxy) {
    }

    public override void Enter() {
        base.Enter();

        proxy.PlayExplosionVfx();

        nextState = new FlyingObjectExpiredState(initializer, proxy);
    }

}
