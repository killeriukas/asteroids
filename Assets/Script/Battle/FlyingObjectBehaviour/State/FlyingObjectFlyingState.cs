﻿using TMI.Core;
using UnityEngine;

public class FlyingObjectFlyingState : BaseFlyingObjectState {

    public FlyingObjectFlyingState(IInitializer initializer, IFlyingObjectBehaviour proxy) : base(initializer, proxy) {

    }

    public override void Enter() {
        base.Enter();
        proxy.onTriggerEnter += OnTriggerEntered;
        proxy.TurnOn();
    }

    //collision is sorted through layers
    protected virtual void OnTriggerEntered(Collider2D collider) {
        nextState = proxy.CreateExplodedState();
    }

    public override void Exit() {
        proxy.onTriggerEnter -= OnTriggerEntered;
        base.Exit();
    }

}
