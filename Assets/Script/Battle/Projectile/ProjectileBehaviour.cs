﻿using TMI.State;
using UnityEngine;

public class ProjectileBehaviour : BaseFlyingObjectBehaviour, IProjectileBehaviour {

    private GameObjectPool<ProjectileBehaviour> projectilePool;
    private GameObjectPool<VfxVisualAccessor> vfxExplosionPool;

    public void Initialize(GameObjectPool<ProjectileBehaviour> projectilePool,
        Vector2 startingPosition,
        Vector2 forward,
        GameObjectPool<VfxVisualAccessor> vfxExplosionPool) {
        base.Initialize(startingPosition, forward);

        this.projectilePool = projectilePool;
        this.vfxExplosionPool = vfxExplosionPool;
    }

    protected override IStateMachine CreateStateMachine() {
        return StateMachine.Create(new ProjectileFlyingState(initializer, this));
    }

    public override void TurnOff() {
        projectilePool.Release(this);
        base.TurnOff();
    }

    public override void PlayExplosionVfx() {
        VfxVisualAccessor vfxVisualAccessor = vfxExplosionPool.Get();
        vfxVisualAccessor.transform.position = transform.position;
        vfxVisualAccessor.Initialize(vfxExplosionPool);

        battleManager.sfxProjectileExplosion.Play();
    }

    public override BaseFlyingObjectState CreateExplodedState() {
        return new FlyingObjectExplodedState(initializer, this);
    }
}