﻿using TMI.Core;
using TMI.State;

public abstract class BaseProjectileState : BaseStateWithProxy<IProjectileBehaviour> {

    protected BaseProjectileState(IInitializer initializer, IProjectileBehaviour proxy) : base(initializer, proxy) {
    }

}
