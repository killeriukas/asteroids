﻿using TMI.Core;

public class ProjectileExpiredState : BaseProjectileState {

    public ProjectileExpiredState(IInitializer initializer, IProjectileBehaviour proxy) : base(initializer, proxy) {
    }

    public override void Enter() {
        base.Enter();
        proxy.TurnOff();
    }

}
