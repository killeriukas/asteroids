﻿using TMI.Core;

public class ProjectileExplodedState : BaseProjectileState {

    public ProjectileExplodedState(IInitializer initializer, IProjectileBehaviour proxy) : base(initializer, proxy) {
    }

    public override void Enter() {
        base.Enter();

        //play vfx maybe

        nextState = new ProjectileExpiredState(initializer, proxy);
        //Trigger(new ProjectileExplodedNotification(proxy));
    }

}
