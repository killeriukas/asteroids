﻿using System;
using TMI.Core;
using UnityEngine;

public class ProjectileFlyingState : FlyingObjectFlyingState {

    private static TimeSpan projectileLifeSpan = TimeSpan.FromSeconds(1);

    public ProjectileFlyingState(IInitializer initializer, IProjectileBehaviour proxy) : base(initializer, proxy) {

    }

    protected override void OnTriggerEntered(Collider2D collider) {
        base.OnTriggerEntered(collider);
        proxy.mainCollider.enabled = false;
    }

    public override void Update() {
        base.Update();

        if(timeInState > projectileLifeSpan) {
            nextState = new FlyingObjectExpiredState(initializer, proxy);
        }

    }

}
