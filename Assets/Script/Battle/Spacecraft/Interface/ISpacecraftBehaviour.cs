﻿using System;
using TMI.Pattern;
using UnityEngine;

public interface ISpacecraftBehaviour : IProxy {

    event Action<Collider2D> onTriggerEnter;
    SpacecraftSettings spacecraftSettings { get; }
    void Shoot();
    void ResetSpacecraft();
    Animator animator { get; }
    Collider2D mainCollider { get; }
    ParticleSystem vfxThrust { get; }
    AudioSource sfxThrust { get; }
}
