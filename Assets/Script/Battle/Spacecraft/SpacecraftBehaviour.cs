﻿using System;
using TMI.Core;
using TMI.Helper;
using TMI.Notification;
using TMI.State;
using UnityEngine;

public class SpacecraftBehaviour : BaseNotificationBehaviour, ISpacecraftBehaviour, IUpdatable {

    [SerializeField]
    private Rigidbody2D _rigidbody2D;

    [SerializeField]
    private Collider2D _collider;

    [SerializeField]
    private Animator _animator;

    [SerializeField]
    private ParticleSystem _vfxThrust;

    [SerializeField]
    private SpacecraftVisualAccessor visualAccessor;

    [SerializeField]
    private GameObject vfxExplostionPrefab;

    [SerializeField]
    private GameObject projectilePrefab;

    [SerializeField]
    private AudioSource _sfxThrust;

    [SerializeField]
    private AudioSource _sfxCrash;

    [SerializeField]
    private AudioSource _sfxShoot;

    private GameObjectPool<ProjectileBehaviour> projectilePool;
    private GameObjectPool<VfxVisualAccessor> vfxExplosionPool;

    private IStateMachine stateMachine;
    private IExecutionManager executionManager;
    private BattleManager battleManager;

    public event Action<Collider2D> onTriggerEnter;

    public SpacecraftSettings spacecraftSettings { get; private set; } = new SpacecraftSettings();
    public Animator animator => _animator;
    public Collider2D mainCollider => _collider;
    public ParticleSystem vfxThrust => _vfxThrust;
    public AudioSource sfxThrust => _sfxThrust;

    public override void Setup(IInitializer initializer) {
        base.Setup(initializer);
        executionManager = initializer.GetManager<ExecutionManager, IExecutionManager>();
        battleManager = initializer.GetManager<BattleManager>();
        
        const int projectilePoolSize = 20;
        projectilePool = new GameObjectPool<ProjectileBehaviour>(initializer, projectilePrefab, projectilePoolSize);
        vfxExplosionPool = new GameObjectPool<VfxVisualAccessor>(initializer, vfxExplostionPrefab, projectilePoolSize);

        stateMachine = StateMachine.Create(new SpacecraftSpawnState(initializer, this));

        executionManager.Register(this, OnUpdate);
        
        Listen<SpacecraftExplodedNotification>(this, OnSpacecraftExploded);
    }

    public void ResetSpacecraft() {
        spacecraftSettings.currentAcceleration = Vector2.zero;
        spacecraftSettings.currentRotation = Quaternion.identity;

        _rigidbody2D.velocity = Vector2.zero;
        _rigidbody2D.position = Vector2.zero;
    }

    private void OnSpacecraftExploded(SpacecraftExplodedNotification notification) {
        VfxVisualAccessor vfxVisualAccessor = vfxExplosionPool.Get();
        vfxVisualAccessor.transform.position = transform.position;

        vfxVisualAccessor.Initialize(vfxExplosionPool);

        _sfxCrash.Play();
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        onTriggerEnter?.Invoke(collision);
    }

    private ExecutionManager.Result OnUpdate() {
        stateMachine.Update();
        return ExecutionManager.Result.Continue;
    }

    public void Shoot() {
        ProjectileBehaviour projectile = projectilePool.Get();
        projectile.Initialize(projectilePool,
            visualAccessor.gunTransform.position,
            transform.up,
            vfxExplosionPool);

        _sfxShoot.Play();
    }

    private void FixedUpdate() {
        _rigidbody2D.SetRotation(spacecraftSettings.currentRotation);

        _rigidbody2D.AddForce(spacecraftSettings.currentAcceleration, ForceMode2D.Force);

        Vector2 newVelocity = _rigidbody2D.velocity;
        Vector2 maxVelocity = spacecraftSettings.maxVelocity;

        newVelocity.x = Mathf.Clamp(newVelocity.x, -maxVelocity.x, maxVelocity.x);
        newVelocity.y = Mathf.Clamp(newVelocity.y, -maxVelocity.y, maxVelocity.y);

        _rigidbody2D.velocity = newVelocity;

        //if position is outside of screen, mirror it to the other side
        Vector2 newPosition;
        bool isMirrorRequired = battleManager.CalculateMirrorPositionOnEdge(_rigidbody2D.position, Vector2.zero, out newPosition);
        if(isMirrorRequired) {
            _rigidbody2D.position = newPosition;
        }

    }

    protected override void OnDestroy() {
        executionManager.Unregister(this);
        GeneralHelper.DisposeAndMakeDefault(ref stateMachine);
        GeneralHelper.DisposeAndMakeDefault(ref projectilePool);
        GeneralHelper.DisposeAndMakeDefault(ref vfxExplosionPool);
        base.OnDestroy();
    }

}