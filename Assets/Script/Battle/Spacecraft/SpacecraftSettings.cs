﻿using UnityEngine;

public class SpacecraftSettings {
    public Vector2 maxVelocity = new Vector2(25f, 25f);
    public const float maxAcceleration = 3750f;
    public Vector2 currentAcceleration = Vector2.zero;

    public Quaternion currentRotation = Quaternion.identity;
    public const float maxRotationSpeedDegrees = 180f;


}
