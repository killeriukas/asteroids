﻿using TMI.Core;
using TMI.State;

public abstract class BaseSpacecraftState : BaseStateWithProxy<ISpacecraftBehaviour> {

    protected BaseSpacecraftState(IInitializer initializer, ISpacecraftBehaviour proxy) : base(initializer, proxy) {
    }

}
