﻿using TMI.Core;

public class SpacecraftDeathState : BaseSpacecraftState {

    public SpacecraftDeathState(IInitializer initializer, ISpacecraftBehaviour proxy) : base(initializer, proxy) {
    }

    public override void Enter() {
        base.Enter();
        proxy.mainCollider.enabled = false;
        proxy.ResetSpacecraft();
    }
}
