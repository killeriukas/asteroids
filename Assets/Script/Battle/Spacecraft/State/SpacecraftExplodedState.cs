﻿using TMI.Core;

public class SpacecraftExplodedState : BaseSpacecraftState {

    private BattleManager battleManager;

    public SpacecraftExplodedState(IInitializer initializer, ISpacecraftBehaviour proxy) : base(initializer, proxy) {
        this.battleManager = initializer.GetManager<BattleManager>();
    }

    public override void Enter() {
        base.Enter();

        proxy.animator.SetTrigger("death");

        Trigger(new SpacecraftExplodedNotification());

        IPlayerProfile playerProfile = battleManager.playerProfile;
        if(playerProfile.healthAmount.value > 0) {
            nextState = new SpacecraftRespawnState(initializer, proxy);
        } else {
            nextState = new SpacecraftDeathState(initializer, proxy);
        }
    }

}
