﻿using System;
using TMI.Core;
using TMI.Helper;
using TMI.State;
using UnityEngine;

public class SpacecraftFlyingState : BaseSpacecraftState {

    private IStateMachine stateMachine;

    public SpacecraftFlyingState(IInitializer initializer, ISpacecraftBehaviour proxy) : base(initializer, proxy) {
        this.stateMachine = StateMachine.Create(new SpacecraftInvincibilitySubstate(initializer, proxy));
    }

    public override void Enter() {
        base.Enter();
        proxy.onTriggerEnter += OnTriggerEntered;
    }

    //collision is sorted through layers
    private void OnTriggerEntered(Collider2D collider) {
        proxy.mainCollider.enabled = false;
        nextState = new SpacecraftExplodedState(initializer, proxy);
    }

    public override void Update() {
        base.Update();
        stateMachine.Update();

        if(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)) {
            proxy.vfxThrust.Play(true);
            proxy.sfxThrust.Play();
        }

        if(Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.UpArrow)) {
            proxy.vfxThrust.Stop(true);
            proxy.sfxThrust.Stop();
        }

        EvaluateMovement();

        if(Input.GetKeyDown(KeyCode.Space)) {
            proxy.Shoot();
        }

    }

    private void EvaluateMovement() {
        //first calculate rotation, cuz it affects the forward vector
        float rotationAcceleration = Input.GetAxis("Horizontal");
        float rotationPerSecond = Time.deltaTime * rotationAcceleration * SpacecraftSettings.maxRotationSpeedDegrees;

        Quaternion currentRotation = proxy.spacecraftSettings.currentRotation;
        Quaternion newRotation = currentRotation * Quaternion.Euler(0, 0, -rotationPerSecond);

        proxy.spacecraftSettings.currentRotation = newRotation;

        //clamp because can't accelerate backward
        float verticalAcceleration = Mathf.Clamp01(Input.GetAxis("Vertical"));

        Vector2 currentForward = newRotation * Vector2.up;

        Vector2 inputAcceleration = currentForward * verticalAcceleration * Time.deltaTime;
        inputAcceleration *= SpacecraftSettings.maxAcceleration;
        proxy.spacecraftSettings.currentAcceleration = inputAcceleration;
    }

    public override void Exit() {
        proxy.vfxThrust.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
        proxy.sfxThrust.Stop();
        proxy.onTriggerEnter -= OnTriggerEntered;
        base.Exit();
    }

    public override void Dispose() {
        GeneralHelper.DisposeAndMakeDefault(ref stateMachine);
        base.Dispose();
    }

}
