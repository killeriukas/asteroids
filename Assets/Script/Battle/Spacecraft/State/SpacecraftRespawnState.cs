﻿using System;
using TMI.Core;

public class SpacecraftRespawnState : BaseSpacecraftState {

    private static readonly TimeSpan respawnDuration = TimeSpan.FromSeconds(2);

    public SpacecraftRespawnState(IInitializer initializer, ISpacecraftBehaviour proxy) : base(initializer, proxy) {
    }

    public override void Update() {
        base.Update();

        if(timeInState > respawnDuration) {
            nextState = new SpacecraftSpawnState(initializer, proxy);
        }
    }

}
