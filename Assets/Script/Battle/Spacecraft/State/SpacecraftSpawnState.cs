﻿using TMI.Core;

public class SpacecraftSpawnState : BaseSpacecraftState {

    public SpacecraftSpawnState(IInitializer initializer, ISpacecraftBehaviour proxy) : base(initializer, proxy) {

    }

    public override void Enter() {
        base.Enter();
        proxy.ResetSpacecraft();

        nextState = new SpacecraftFlyingState(initializer, proxy);
    }

}
