﻿using TMI.Core;
using TMI.State;

public abstract class BaseSpacecraftSubstate : BaseStateWithProxy<ISpacecraftBehaviour> {

    protected BaseSpacecraftSubstate(IInitializer initializer, ISpacecraftBehaviour proxy) : base(initializer, proxy) {

    }

}
