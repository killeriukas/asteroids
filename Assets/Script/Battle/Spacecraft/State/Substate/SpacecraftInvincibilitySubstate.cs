﻿using System;
using TMI.Core;

public class SpacecraftInvincibilitySubstate : BaseSpacecraftSubstate {

    private static readonly TimeSpan invincibilityDuration = TimeSpan.FromSeconds(3);

    public SpacecraftInvincibilitySubstate(IInitializer initializer, ISpacecraftBehaviour proxy) : base(initializer, proxy) {
    }

    public override void Enter() {
        base.Enter();
        proxy.mainCollider.enabled = false;
        proxy.animator.SetBool("spawning", true);
    }

    public override void Update() {
        base.Update();

        if(timeInState > invincibilityDuration) {
            nextState = new SpacecraftNormalSubstate(initializer, proxy);
        }
    }

    public override void Exit() {
        proxy.mainCollider.enabled = true;
        proxy.animator.SetBool("spawning", false);
        base.Exit();
    }

}
