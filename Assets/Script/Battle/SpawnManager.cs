﻿using System.Collections.Generic;
using TMI.Core;
using TMI.Helper;
using UnityEngine;

public class SpawnManager : BaseNotificationManager {

    private Dictionary<AsteroidBehaviour.Size, GameObjectPool<AsteroidBehaviour>> sizeToPoolMapping = new Dictionary<AsteroidBehaviour.Size, GameObjectPool<AsteroidBehaviour>>();

    private GameObjectPool<AsteroidBehaviour> bigAsteroids;
    private GameObjectPool<AsteroidBehaviour> mediumAsteroids;
    private GameObjectPool<AsteroidBehaviour> smallAsteroids;

    private int startingAsteroidCount = 5;
    private int totalAsteroidsLeft;

    public override void Setup(IInitializer initializer, bool isNew) {
        base.Setup(initializer, isNew);
        Listen<AsteroidExplodedNotification>(this, OnAsteroidExploded);
    }

    private void OnAsteroidExploded(AsteroidExplodedNotification notification) {
        --totalAsteroidsLeft;

        if(0 == totalAsteroidsLeft) {
            ++startingAsteroidCount;
            Trigger(new AllAsteroidsDestroyedNotification());
        }
    }

    public void SpawnAsteroids(Vector2 bounds) {
        SpawnAsteroids(bounds, startingAsteroidCount);
    }

    public void SpawnAsteroids(Vector2 bounds, int bigAsteroidAmount) {
        const int totalRocksInBigAsteroid = 7;
        totalAsteroidsLeft = bigAsteroidAmount * totalRocksInBigAsteroid; 

        for(int i = 0; i < bigAsteroidAmount; ++i) {
            Vector2 startingPosition = CalculateStartingPosition(bounds);
            Quaternion randomRotation = Quaternion.Euler(0, 0, Random.Range(0f, 360f));
            Vector2 randomDirection = randomRotation * Vector2.up;

            AsteroidBehaviour asteroidBehaviour = bigAsteroids.Get();
            asteroidBehaviour.Initialize(startingPosition, randomDirection, bigAsteroids);
        }

    }

    private Vector2 CalculateStartingPosition(Vector2 bounds) {
        bool isHorizontal = Random.Range(0, 2) > 0;
        
        if(isHorizontal) {
            bool isRight = Random.Range(0, 2) > 0;
            float yPosition = Random.Range(-bounds.y, bounds.y);
            return new Vector2(isRight ? bounds.x : -bounds.x, yPosition);
        } else {
            bool isUp = Random.Range(0, 2) > 0;
            float xPosition = Random.Range(-bounds.x, bounds.x);
            return new Vector2(xPosition, isUp ? bounds.y : -bounds.y);
        }
    }


    public void Initialize(SpawnAccessor spawnAccessor, int amount) {
        //all asteroids split into two, hence the multiplacation
        bigAsteroids = new GameObjectPool<AsteroidBehaviour>(initializer, spawnAccessor.bigAsteroidPrefab, amount);
        mediumAsteroids = new GameObjectPool<AsteroidBehaviour>(initializer, spawnAccessor.mediumAsteroidPrefab, amount * 2);
        smallAsteroids = new GameObjectPool<AsteroidBehaviour>(initializer, spawnAccessor.smallAsteroidPrefab, amount * 4);

        sizeToPoolMapping.Add(AsteroidBehaviour.Size.Big, bigAsteroids);
        sizeToPoolMapping.Add(AsteroidBehaviour.Size.Medium, mediumAsteroids);
        sizeToPoolMapping.Add(AsteroidBehaviour.Size.Small, smallAsteroids);
    }

    public void SpawnSplitAsteroids(AsteroidBehaviour.Size size, Vector2 position) {
        const int newAsteroidAmount = 2;

        GameObjectPool<AsteroidBehaviour> asteroidsPool = sizeToPoolMapping[size];

        for(int i = 0; i < newAsteroidAmount; ++i) {
            AsteroidBehaviour asteroid = asteroidsPool.Get();

            Vector2 randomDirection = Random.insideUnitCircle.normalized;
            asteroid.Initialize(position, randomDirection, asteroidsPool);
        }

    }

    protected override void OnDestroy() {
        sizeToPoolMapping.Clear();
        GeneralHelper.DisposeAndMakeDefault(ref bigAsteroids);
        GeneralHelper.DisposeAndMakeDefault(ref mediumAsteroids);
        GeneralHelper.DisposeAndMakeDefault(ref smallAsteroids);
        base.OnDestroy();
    }

}