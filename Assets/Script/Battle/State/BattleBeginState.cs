﻿using TMI.Core;

public class BattleBeginState : BaseBattleState {

    private SpawnManager spawnManager;
    private BattleManager battleManager;

    public BattleBeginState(IInitializer initializer) : base(initializer) {
        this.spawnManager = initializer.GetManager<SpawnManager>();
        this.battleManager = initializer.GetManager<BattleManager>();
    }

    public override void Enter() {
        base.Enter();
        spawnManager.SpawnAsteroids(battleManager.halfArenaSize);

        nextState = new BattleFightState(initializer);
    }

}
