﻿using TMI.Core;

public class BattleFightState : BaseBattleState {

    private BattleManager battleManager;
    private IPlayerProfile playerProfile => battleManager.playerProfile;

    public BattleFightState(IInitializer initializer) : base(initializer) {
        this.battleManager = initializer.GetManager<BattleManager>();
    }

    public override void Enter() {
        base.Enter();

        Listen<AllAsteroidsDestroyedNotification>(this, OnAllAsteroidsDestroyed);
        Listen<AllPlayerLivesLostNotification>(this, OnAllPlayerLivesLost);
    }

    private void OnAllPlayerLivesLost(AllPlayerLivesLostNotification notification) {
        nextState = new BattleFinishState(initializer);
    }

    private void OnAllAsteroidsDestroyed(AllAsteroidsDestroyedNotification notification) {
        if(playerProfile.healthAmount.value > 0) {
            nextState = new BattleCountdownState(initializer);
        }
    }
}
