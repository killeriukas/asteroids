﻿using System;
using TMI.Core;

public class BattleFinishState : BaseBattleState {

    private static readonly TimeSpan finishDuration = TimeSpan.FromSeconds(5);

    public BattleFinishState(IInitializer initializer) : base(initializer) {
    }

    public override void Update() {
        base.Update();

        if(timeInState > finishDuration) {
            nextState = new BattleRestartState(initializer);
        }
    }

}
