﻿using TMI.AssetManagement;
using TMI.Core;
using TMI.UI;

public class LandingInitializer : BaseCacheUIMiniInitializer {

    protected override ISceneGroup CreateUIScenesCache() {
        ISceneGroup sceneGroup = SceneGroup.Create();
        sceneGroup.Add("ui_scene_loading");
        return sceneGroup;
    }

    protected override void OnUIScenesCached() {
        LoadingScreenUIController loadingScreen = uiManager.LoadUI<LoadingScreenUIController>();
        loadingScreen.Show();

        sceneManager.LoadScene(SceneConstants.metagame);
    }

}