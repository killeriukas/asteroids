﻿using TMI.Core;
using TMI.AssetManagement;
using TMI.UI;

public class MetagameInitializer : BaseCacheUIMiniInitializer {

    protected override ISceneGroup CreateUIScenesCache() {
        ISceneGroup sceneGroup = SceneGroup.Create();
        sceneGroup.Add("ui_scene_metagame");
        return sceneGroup;
    }

    protected override void OnUIScenesCached() {
        MetagameUIController metagameUIController = uiManager.LoadUI<MetagameUIController>();
        metagameUIController.Show();

        LoadingScreenUIController loadingScreen = uiManager.LoadUI<LoadingScreenUIController>();
        loadingScreen.Hide();
    }

    protected override void OnDestroy() {
        uiManager.UnloadUIScene("ui_scene_metagame");
        base.OnDestroy();
    }

}
