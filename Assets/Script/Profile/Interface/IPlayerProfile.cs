﻿using TMI.Pattern;

public interface IPlayerProfile {
    ISubject<int> highScore { get; }
    string scoreDescription { get; }
    ISubject<int> healthAmount { get; }
    string gameOverScoreDescription { get; }
}
