﻿
using TMI.Pattern;

public class PlayerProfile : IPlayerProfile {

    private Subject<int> _highScore = new Subject<int>();
    public ISubject<int> highScore => _highScore;

    public void AddScore(int score) {
        _highScore.value += score;
    }

    private Subject<int> _healthAmount = new Subject<int>();
    public ISubject<int> healthAmount => _healthAmount;

    public void ConsumeHealth() {
        --_healthAmount.value;
    }

    public PlayerProfile(int healthAmount) {
        this._highScore.value = 0;
        this._healthAmount.value = healthAmount;
    }

    public string scoreDescription => "Score: " + _highScore;
    public string gameOverScoreDescription => "Game Over\nYour Score: " + _highScore;

}
