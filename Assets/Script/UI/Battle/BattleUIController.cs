﻿using System;
using System.Collections.Generic;
using TMI.Core;
using TMI.UI;
using TMI.UI.Extension;
using UnityEngine;

public class BattleUIController : BaseUIController {

    [SerializeField]
    private UIList healthAmountList;

    [SerializeField]
    private UITextPro highScoreText;

    [SerializeField]
    private UITextPro gameOverHighScoreText;

    private BattleManager battleManager;
    private IPlayerProfile playerProfile => battleManager.playerProfile;

    private List<Cell> healthCellList = new List<Cell>();

    public override void Setup(IInitializer initializer) {
        base.Setup(initializer);
        battleManager = initializer.GetManager<BattleManager>();

        highScoreText.text = playerProfile.scoreDescription;

        for(int i = 0; i < playerProfile.healthAmount.value; ++i) {
            Cell cell = healthAmountList.CloneCell();
            healthCellList.Add(cell);
        }

        playerProfile.highScore.onChange += OnHighScoreChanged;
        playerProfile.healthAmount.onChange += OnHealthAmountChanged;

        gameOverHighScoreText.gameObject.SetActive(false);
        Listen<AllPlayerLivesLostNotification>(this, OnAllPlayerLivesLost);
    }

    private void OnAllPlayerLivesLost(AllPlayerLivesLostNotification notification) {
        gameOverHighScoreText.gameObject.SetActive(true);
    }

    private void OnHealthAmountChanged(int oldValue, int newValue) {
        Cell cell = healthCellList[0];
        healthCellList.RemoveAt(0);
        healthAmountList.DestroyCell(cell);
    }

    private void OnHighScoreChanged(int oldValue, int newValue) {
        highScoreText.text = playerProfile.scoreDescription;
        gameOverHighScoreText.text = playerProfile.gameOverScoreDescription;
    }

    protected override void OnDestroy() {
        playerProfile.highScore.onChange -= OnHighScoreChanged;
        playerProfile.healthAmount.onChange -= OnHealthAmountChanged;
        base.OnDestroy();
    }

}